## Comandos Linux



### help
```bash
[comando] --help
```

### man
```bash
man [opções] [comando]
```

### logout
```bash
exit
logout
```

### shutdown
```bash
shutdown -h now
poweroff
halt
```

### reboot
```bash
shutdown -r <tempo>
```


4Linux

`whoami` - Quem é o usuário no momento

### Variáveis
LOCAL="variável local"
export GLOBAL="variável global"

`set`
`env`
`printenv`
`unset`

Manter para todos usuários
/etc/profile
/etc/environment

Manter para usuário atual
~/.bashrc
~/.bash_profile
~/.bash_login4
~/.profile

help [comando interno]

[comando externo] --help

How-to ou cookbook
/usr/share/doc

Comandos
apropos - para procurar algum comando
man -k também procura algum comando

whatis procura mais específico
man -f também procura

Man do 1 ao 9
/usr/share/man

whereis para procurar o binario e a man

which somente para o binario

https://explainshell.com/


Comando `journalctl` para ver logs