
## Atalhos para terminal BASH

`TAB` Completa nome de arquivo, pasta ou programa.

`Ctrl + C` Sair de um comando ou processo. Para imediatamente a execução de um programa.

`Ctrl + Z` Ativar execução de programa em segundo plano.

`Ctrl + D` Desconectar do terminal ou fechar sessão SSH.

`Ctrl + L` Limpar o terminal.

`Ctrl + A` Mover o cursor para o início da linha.

`Ctrl + E` Mover o cursor para o final da linha.

`Alt + B` Uma palavra para trás

`Ctrl + B` Uma letra para trás

`Alt + F` Uma palavra para frente

`Ctrl + F` Uma letra para frente

`Ctrl + xx` Cursor para inicia da linha e volta

`Ctrl + U` Deletar do cursor para o início da linha.

`Ctrl + K` Deletar do cursor para o final da linha.

`Ctrl + W` Deletar uma palavra anterior a posição do cursor.

`Ctrl + Y` Colar texto excluído pelos comandos anteriores.

`Ctrl + P` Visualizar o comando anterior.

`Ctrl + N` Visualizar o próximo comando.

`Ctrl + R` Pesquisar no histórico de comandos.

## Terminator

https://cheatography.com/elpedro/cheat-sheets/terminator/
