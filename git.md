## Comandos GIT

### Arquivos

`git status`

`git add` Adicionar arquivo.

`git rm` Deletar arquivo.

`git commit`

`git push`

`git pull`

### Configuração

`git config --global user.name "Nome"`

`git config --global user.email "Email"`

### Logs

`git log` Visualizar logs.

`git log -3` Visualizar os últimos 3 logs.

`git log --oneline` Visualizar os logs em uma linha.

`git log --author="Diego Becker"` Pesquisa os logs por autor.