Setup máquina Linux

- Ajustar teclado
https://www.danielkossmann.com/ajeitando-cedilha-errado-ubuntu-linux/

https://arthurgregorio.eti.br/blog/hardware/configurar-acentos-para-teclados-em-ingles-no-linux/

Executar ao iniciar: setxkbmap -model pc104 -layout us_intl
https://askubuntu.com/questions/228304/how-do-i-run-a-script-at-start-up

Criar um script em /etc com : setxkbmap -model pc104 -layout us_intl
Colocar o script em Startup Applications


- Criar atalho para mover monitor
https://github.com/jc00ke/move-to-next-monitor
https://unix.stackexchange.com/questions/48456/xfce-send-window-to-other-monitor-on-keystroke

-Softwares

    • 1password
        ◦ wget https://downloads.1password.com/linux/debian/amd64/stable/1password-latest.deb
        ◦ sudo dpkg -i 1password-latest.deb
    • Terminator com oh-my-bash
        ◦ sudo apt-get install terminator
        ◦ https://github.com/ohmybash/oh-my-bash
    • VSCodium
        ◦ wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg
    | gpg --dearmor
    | sudo dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg
        ◦ echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main'
    | sudo tee /etc/apt/sources.list.d/vscodium.list
        ◦ sudo apt update && sudo apt install codium
    • Virtualbox
        ◦ wget https://download.virtualbox.org/virtualbox/6.1.26/virtualbox-6.1_6.1.26-145957~Debian~bullseye_amd64.deb
        ◦ sudo apt-get install -y libqt5opengl5
        ◦ sudo dpkg -i virtualbox-6.1_6.1.26-145957~Debian~bullseye_amd64.deb
        ◦ wget https://download.virtualbox.org/virtualbox/6.1.26/Oracle_VM_VirtualBox_Extension_Pack-6.1.26.vbox-extpack
        ◦ sudo apt update
        ◦ sudo VBoxManage extpack install Oracle_VM_VirtualBox_Extension_Pack-6.1.26.vbox-extpack
    • Vagrant
        ◦ wget https://releases.hashicorp.com/vagrant/2.2.18/vagrant_2.2.18_x86_64.deb
        ◦ sudo dpkg -i vagrant_2.2.18_x86_64.deb
    • Docker
        ◦ curl -fsSL https://get.docker.com/ | bash
    • Vim
        ◦ sudo apt install vim
    Gcloug
    https://cloud.google.com/sdk/docs/install#deb
    Kubeadm, kubelet e kubectl
    https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/